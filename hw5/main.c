#include <stdio.h>

#define N1 10
#define N2 10
#define N  20

void printpoly(int, const int []);

int* multpoly(int dest[],
              int n1, const int poly1[],
              int n2, const int poly2[]);

int main()
{
    int poly1[N1];
    int poly2[N2];
    int poly[N] = {};

    for (int i=0; i<N1; ++i) scanf("%d", &poly1[i]);
    for (int i=0; i<N2; ++i) scanf("%d", poly2 + i);
    multpoly(poly, N1, poly1, N2, poly2);

    printf("  "); printpoly(N1, poly1); printf("\n");
    printf("* "); printpoly(N2, poly2); printf("\n");
    printf("------------------------------------------------------------\n");
    printf("  "); printpoly(N, poly); printf("\n");

    return 0;
}

int allz(int n, const int* p)
{
    int res = 0;
    while (n--) res |= *p++;
    return !res;
}

void printpoly(int n, const int poly[])
{
    if (allz(n,poly)) { printf("0"); return; }

    int first = 1;
    for (int i=0; i<n; ++i)
    {
        if (0 == poly[i]) continue;

        if (0  < poly[i])
        {
            if (first) first=0; else printf(" +");
            if (i==0) { printf("%d", poly[i]); continue; }
            if (1!=poly[i]) printf("%d",poly[i]);
        }

        if (0  > poly[i])
        {
            if (first) first=0; else printf(" ");
            if (i==0) { printf("%d", poly[i]); continue; }
            if (-1==poly[i]) printf("-"); else printf("%d",poly[i]);
        }

        switch (i) {
            case 0: break;
            case 1: printf("x"); break;
            default: printf("x^%d", i);
        }

    }
}

int* multpoly(int dest[], int n1, const int poly1[], int n2, const int poly2[])
{
    /*
    // dummy implementation just copying from poly1
    for(int i=0; i<n1; ++i) dest[i] = poly1[i];
    */
    for(int i=0; i<n1; ++i)
        for(int j=0; j<n2; ++j)
            dest[i+j] += poly1[i] * poly2[j];

    return dest;
}
